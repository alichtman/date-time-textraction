### Date-Time-Textraction

Extract simple date and time expressions from a block of text.

Capture expressions like the following:

```
01/10/2011
Thursday August 3rd, 1960
the 21st of December
Friday, January 25th
Monday
Monday the 23rd
Martin Luther King Day
Thanksgiving
Labor Day
```

Each expression is extracted in the longest possible form. That means that if the text says `.., Feb. 14th, 2012...`, `Feb. 14th, 2012` is the only chunk that will be extracted, not `Feb. 14th` or `2012`.

It recognizes all "absolute" dates, but not relative dates like `the day before yesterday`.

It recognizes major holidays, such as `Labor Day`, `Memorial Day`, `Christmas`, etc.

## Usage
---

The program takes two arguments, an input text file (`input.txt`) and an output text file.

0. Download repo as `.zip`
1. `cd` into the main directory
2. Populate the `input.txt` file
3. `$ python3 date-time-textraction.py input.txt output.txt`
 
