# author: alichtman

import os
import re
import sys
from pprint import pprint

# Command line argument validation
if not len(sys.argv) == 3:
	print("Invalid command line arguments...")
	print("Please enter: `python3 date_parser.py input_filename output_filename`")
	sys.exit(0)

# Extract input and longest_matches file names
input_filepath = os.path.join(os.getcwd(), "input/{}".format(sys.argv[1]))
output_filepath = os.path.join(os.getcwd(), "output/{}".format(sys.argv[2]))

# Read from input file
input_text = open(input_filepath, "r", encoding='utf-8')

#######
# REGEX
#######

weekdays = "(Monday|Mon(\.)?|Tuesday|Tues?(\.)?|Wednesday|Weds?(\.)?|Thursday|Thurs?(\.)?|Friday|Fri(\.)?|Saturday|Sat(\.)?|Sunday|Sun(\.)?)"
months = "(January|February|March|April|May|June|July|August|September|October|November|December|Jan(\.)?|Feb(\.)?|Mar(\.)?|Apr(\.)?|Jun(\.)?|Jul(\.)?|Aug(\.)?|Sep(\.)?|Sept(\.)?|Oct(\.)?|Nov(\.)?|Dec(\.)?)"
holiday_pattern = r"(((George )?Washington's Birthday)|((Memorial|Independence|Labor|Columbus|Veterans|Thanksgiving|Christmas|New Year's|MLK|Martin Luther King) Day)|(Thanksgiving|Christmas|New Year's))"
american_date_pattern = r"(\b\d{1,2}\/\d{1,2}\/\d{2,4}\b)"

# Day of the week, the #(st/nd/rd/th)
articles_for_dates_patterns = r"(the )?\d{1,2}(st|nd|rd|th) of " + months
weekday_number_patterns = r"" + weekdays + "(((,)? " + articles_for_dates_patterns + ")?)"
month_year_patterns = r"" + months + " ?(\d{4}|'\d{2})?"
year_pattern = r"(\b\d{4}\b)"

# Day of the week, Month #(st/nd/rd/th), Year
day_month_suffix_year_patterns = r"(" + weekdays + "(,)? )?" + months + " (\d{1,2}(st|nd|rd|th)?)?((,)? \d{4})?"

longest_matches = []

for line in input_text:
	while 1:
		line_matches = []

		# Compile and search all regex patterns
		american_date_match = re.compile(american_date_pattern).search(line)
		holiday_match = re.compile(holiday_pattern).search(line)
		weekday_number_match = re.compile(weekday_number_patterns).search(line)
		articles_for_dates_match = re.compile(articles_for_dates_patterns).search(line)
		month_year_match = re.compile(month_year_patterns).search(line)
		day_month_suffix_year_match = re.compile(day_month_suffix_year_patterns).search(line)
		year_match = re.compile(year_pattern).search(line)

		# Append tuple of group and span to line_matches list
		if american_date_match:
			line_matches.append((american_date_match.group(0), american_date_match.span(0)))
		if holiday_match:
			line_matches.append((holiday_match.group(0), holiday_match.span(0)))
		if weekday_number_match:
			line_matches.append((weekday_number_match.group(0), weekday_number_match.span(0)))
		if articles_for_dates_match:
			line_matches.append((articles_for_dates_match.group(0), articles_for_dates_match.span(0)))
		if month_year_match:
			line_matches.append((month_year_match.group(0), month_year_match.span(0)))
		if day_month_suffix_year_match:
			line_matches.append((day_month_suffix_year_match.group(0), day_month_suffix_year_match.span(0)))
		if year_match:
			line_matches.append((year_match.group(0), year_match.span(0)))

		# Add longest match by string length to longest_matches and substring it out
		if len(line_matches) > 0:
			longest_match = max(line_matches, key=lambda x:len(x[0]))
			start_idx = longest_match[1][0]
			end_idx = longest_match[1][1]
			longest_matches.append(longest_match[0])
			line = line[:start_idx] + line[end_idx:]
		else:
			break

# https://stackoverflow.com/a/3463143
deduped_longest_matches = [a for a,b in zip(longest_matches, longest_matches[1:]+[not longest_matches[-1]]) if a != b]

# pprint(longest_matches)
pprint(deduped_longest_matches)

# Write to output file
output_file = open(output_filepath,'w', encoding='utf-8')
for item in deduped_longest_matches:
	print(item)
	output_file.write(item + "\n")
output_file.close()


#########
# TESTING
#########

# for line in input_text:
# 	# print(line)
# 	match = complete_compiled_regex.search(line)
# 	# print("Line: " + str(line).strip())
# 	# print("Match: " + str(match))
# 	# print("Match group: " + str(match.group()))
# 	# print("")
# 	if match is not None and match.group() is not "":
# 		# print("MATCH:",match.group())
# 		longest_matches.append(match.group())
# 	# else:
# 	# 	print("DID NOT APPEND:",match.group())

# pprint(longest_matches)

# ## Write longest_matches to output file

# matches_file = open(matches_filename,'w')
# for item in longest_matches:
# 	matches_file.write(item + "\n")

# matches_file.close()

########## TESTS FOR INDIVIDUAL REGULAR EXPRESSIONS

### TEST HOLIDAYS REGEX

# longest_matches = []

# holidays_regex = re.compile(holidays_pattern)
# input_text = open(input_filename, "r")

# print("HOLIDAYS REGEX")
# for line in input_text:
# 	# print(line)
# 	match = holidays_regex.search(line)
# 	if match is not None and match.group() is not "":
# 		# print("MATCH:",match.group())
# 		longest_matches.append(match.group())
# 	# else:
# 	# 	print("DID NOT APPEND:",match.group())

# pprint(longest_matches)
# for item in longest_matches:
# 	matches_file.write(item + "\n")

### TEST DATE REGEX

# longest_matches = []

# date_regex = re.compile(american_date_pattern)
# input_text = open(input_filename, "r")

# print("DATE REGEX")
# for line in input_text:
# 	# print(line)
# 	match = re.search(date_regex, line)
# 	if match is not None and match.group() is not "":
# 		# print("MATCH:",match.group())
# 		longest_matches.append(match.group())
# 	# else:
# 	# 	print("DID NOT APPEND:",match.group())

# pprint(longest_matches)
# for item in longest_matches:
# 	matches_file.write(item + "\n")

### TEST DAY/MONTH REGEX

# longest_matches = []

# day_month_kinda_regex = re.compile(day_month_kinda_pattern)
# input_text = open(input_filename, "r")

# print("DAY_MONTH_KINDA REGEX")
# for line in input_text:
# 	# print(line)
# 	match = re.search(day_month_kinda_regex, line)
# 	if match is not None and match.group() is not "":
# 		# print("MATCH:",match.group())
# 		longest_matches.append(match.group())
# 	# else:
# 	# 	print("DID NOT APPEND:",match.group())

# pprint(longest_matches)
